<x-app-layout>
    <style>
        form{
            margin-top: 20px;
            width: 400px;
            display: flex;
            flex-direction: column;
        }
        label{
            margin-top: 10px;
        }
        .btn{
            margin-top: 20px;
            border: 1px solid grey;
            height: 50px;
        }
        .errors,.success{
            padding: 20px;
        }
        .errors{
            border: 1px solid red;
        }
        .success{
            border: 1px solid green;
        }
    </style>
    <x-slot name="header">
        @if ($errors->any())
            <div class="errors">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('success'))
            <div class="success">
                {{ session("success") }}
            </div>
        @endif
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Редактирование пластинки <b>{{$element->name}}</b> ({{$element->id}})
        </h2>
        @if($element->deleted_at == null)
            <form action="{{ route('records.destroy', $element->id) }}" method="POST">
                @method("DELETE")
                @csrf
                <button class="btn">Удалить пластинку</button>
            </form>
        @endif
        <form method="POST" action="{{ route('records.update',$element->id) }}">
            @method("PATCH")
            @csrf
            <input type="text" readonly value="Создано {{ $element->created_at }}">
            <input type="text" readonly value="Последнее обновление {{ $element->updated_at }}">
            @if($element->deleted_at != null)
                <input type="text" readonly value="Удален {{ $element->deleted_at }}">
            @endif
            <input type="hidden" name="id" value="{{ $element->id }}">
            <label for="name">Название</label>
            <input type="text" name="name" id="name" value="{{ old('name',$element->name) }}">

            <label for="title">Заголовок</label>
            <input type="text" name="title" id="title" value="{{ old('title',$element->title) }}">

            <label for="description">Описание</label>
            <textarea name="description" id="description" rows="5" cols="45">{{ old('description',$element->description) }}</textarea>

            <label for="genre">Жанр</label>
            <select name="genre_id" id="genre_id">
                @foreach($genres as $genre)
                    <option value="{{ $genre->id}}"
                        @if($genre->id == old('genre_id',$element->genre_id)) selected @endif>
                        {{$genre->name}}
                    </option>
                @endforeach
            </select>

            <label for="executor">Исполнитель</label>
            <input type="text" name="executor" id="executor" value="{{ old('executor',$element->executor) }}">

            <label for="country">Страна</label>
            <input type="text" name="country" id="country" value="{{ old('country',$element->country) }}">
            @if($element->deleted_at == null)
                <input type="submit" class="btn" value="Сохранить">
            @endif

        </form>
    </x-slot>
</x-app-layout>
