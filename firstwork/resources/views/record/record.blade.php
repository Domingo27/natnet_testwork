<x-app-layout>
    <style>
        table{
            margin-top: 50px;
            margin-bottom: 50px;
        }
        table,tr,td{
            border:1px solid grey;
            text-align: center;
            font-size: 14px;
            align-items: center;
        }
        .success{
            border: 1px solid green;
        }
    </style>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <a href="{{ route('records.index') }}">Пластинки</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                @if(session('success'))
                    <div class="success">
                        {{ session("success") }}
                    </div>
                @endif

                @if($page->count() == 0 && $page->lastPage() >= 1)
                    <script> window.location.href = '/dashboard'; </script>
                @endif

                @if($page->count() != 0)

                   <table>
                       <tr>
                           <td>Номер пластинки</td>
                           <td>Название пластинки</td>
                           <td>Жанр</td>
                           <td>Описание</td>
                           <td>Исполнитель</td>
                           <td>Страна</td>
                       </tr>
                       @foreach($page as $element)
                           <tr>
                               <td>{{ $element->id }}</td>
                               <td>{{ $element->name }}</td>
                               <td>{{ $genres[$element->genre_id-1]->name }}</td>
                               <td>{{ $element->description }}</td>
                               <td>{{ $element->executor }}</td>
                               <td>{{ $element->country }}</td>
                               <td><a href="{{route('records.edit',$element->id)}}">Редактировать</a></td>
                           </tr>
                       @endforeach
                   </table>

                    {{ $page->links() }}
                    <br>
                @elseif($page->count() == 0)
                    Записи отсутствуют
                @endif

            </div>
        </div>
    </div>
</x-app-layout>
