<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = ["Рок", "Хип-хоп", "Шансон", "Блюз", "Поп-музыка"];
        $genres = [];

        for($i = 0; $i < count($name); $i++){
            $genres[] = [
                'name' => $name[$i],
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        DB::table('genres')->insert($genres);
    }
}
