<?php

namespace Database\Factories;

use App\Models\Record;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Genre;

class RecordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Record::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $genres = Genre::all();
        $executor = ["Skillet", "Queen", "Bring Me The Horizon", "Cold War Kids", "Wildways"];
        $name = $this->faker->unique()->text(30);
        return [
            'name' => $name,
            'title' => $name,
            'genre_id' => $genres[rand(0,count($genres)-1)],
            'executor' => $executor[rand(0,count($executor)-1)],
            'description' => $this->faker->text,
            'country' => $this->faker->country,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
