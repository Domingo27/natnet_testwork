<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class RecordUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:40|unique:records,name,'.$this->id,
            'title' => 'required|min:5|max:40',
            'description' => 'string|max:250|nullable',
            'genre_id' => 'required',
            'executor' => 'required|min:3|max:40',
            'country' => 'required|min:3|max:64'
        ];
    }
    public function messages()
    {
        return [
            'name.unique' => 'Такое название уже занято',
            'name.required' => 'Название обязательное поле',
            'name.min' => 'Минимальное количество символов поля Название равно :min',
            'name.max' => 'Максимальное количество символов поля Название равно :max',
            'title.required'  => 'Заголовок обязательное поле',
            'title.min' => 'Минимальное количество символов поля Заголовок равно :min',
            'title.max' => 'Максимальное количество символов поля Заголовок равно :max',
            'description.string' => 'Описание должно быть текстовым',
            'description.max' => 'Максимальное количество символов поля Описание равно :max',
            'genre_id.required' => 'Жанр обязательное поле',
            'executor' =>  'Исполнитель обязательное поле',
            'executor.min' => 'Минимальное количество символов поля Исполнитель равно :min',
            'executor.max' => 'Максимальное количество символов поля Исполнитель равно :max',
            'country' =>  'Страна обязательное поле',
            'country.min' => 'Минимальное количество символов поля Страна равно :min',
            'country.max' => 'Максимальное количество символов поля Страна равно :max'
        ];
    }
}
