<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecordUpdateRequest;
use App\Models\Record;
use App\Models\Genre;
use Illuminate\Http\Request;


class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Record::paginate(5);
        $genres = Genre::all();
        return view("record.record", compact('page','genres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $element = Record::FindOrFail($id)->withTrashed();
        $element = Record::withTrashed()->findOrFail($id);
        $genres = Genre::all();

        return view('record.edit', compact('element','genres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecordUpdateRequest $request, $id)
    {
        $element = Record::Find($id);
        if(empty($element)){
            return back()
                ->withErrors(['msg' => "Пластинка не найдена"])
                ->withInput();
        }
        $data = $request->all();

        $result = $element
            ->fill($data)
            ->save();
        if($result){
           return redirect()
               ->route('records.edit', $element->id)
               ->with(['success'=>'Данные сохранены']);
        }
        else{
            return back()
                ->withErrors(['msg' => "Данные не сохранены"])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Record::destroy($id);
        if($result){
            return redirect()
                ->route('records.index')
                ->with(['success'=> "Пластинка id [$id] удалена"]);
        }
        else {
            return back()
                ->withErrors(['msg' => "При удалении возникли ошибки"]);
        }

    }
}
