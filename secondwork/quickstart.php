<?php
require 'vendor/autoload.php';


/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient(){
    $client = new Google_Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
    $client->setAuthConfig('settings/credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    $tokenPath = 'settings/token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

// Если время жизни токена истекло, обновляем его
    if ( $client->isAccessTokenExpired() ) {
        $refreshTokenSaved = $client->getRefreshToken();
        // Обновляем токен
        $client->fetchAccessTokenWithRefreshToken( $refreshTokenSaved );
        // Создаём новую переменную, в которую помещаем новый обновлённый токен
        $accessTokenUpdated = $client->getAccessToken();
        // Добавляем в эту переменную старый refresh токен
        $accessTokenUpdated['refresh_token'] = $refreshTokenSaved;
        // Сохраняем в сессии новый токен и старый refresh токен
        file_put_contents($tokenPath, json_encode($accessTokenUpdated));
        // Устанавливаем новый токен
        $client->setAccessToken($accessTokenUpdated);
    }
    return $client;
}
