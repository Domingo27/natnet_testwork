<?php
header('Content-type: text/html; charset=utf-8');

$db_host = "localhost";
$db_user = "root";
$db_password = "root";
$db_name = "second_work";


try {
    $db = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_password);
}
catch (PDOException $e) {
    die();
}
