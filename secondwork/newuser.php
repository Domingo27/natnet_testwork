<?php
include("settings/config.php");

$name = htmlspecialchars($_POST['name']);
$surname = htmlspecialchars($_POST['surname']);
$age = htmlspecialchars($_POST['age']);

$query = "INSERT INTO `user` (`name`,`surname`,`age`) VALUES (:name, :surname, :age)";
$params = [
    ':name' => $name,
    ':surname' => $surname,
    ':age' => $age
];

$stmt = $db->prepare($query);
if($stmt->execute($params)){
    echo json_encode(array('success' => 'true'));
}
else
    echo json_encode(array('success' => 'false'));