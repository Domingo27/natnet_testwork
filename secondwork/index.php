<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-3.5.1.js"
            integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
            crossorigin="anonymous"></script>
    <style>
        .container{
            width: 1024px;
            margin: 0 auto;
        }
        #form_first{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            width: 400px;
        }
        .form{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .errors{
            border: none;
            margin-top: 10px;
            padding: 10px;
            width: 400px;
        }
    </style>
</head>
<body>
        <div class="container">
            <block class="form">
                <form id="form_first" method="post" action="/">
                    <input type="text" placeholder="Имя" name="name" id="name">
                    <input type="text" placeholder="Фамилия" name="surname" id="surname">
                    <input type="text" placeholder="Возраст" name="age" id="age">
                    <button id="save">Сохранить</button>
                    <a href="unload.php">Выгрузить</a>
                </form>
                <div class="errors"></div>
            </block>
        </div>



<script type="text/javascript">
    function save()
    {
        $(".errors").html("");
        $(".errors").css("border", "none");
        let errors = [];
        if( $("#age").val() <= 0){
            errors.push("Поле Возвраст не может быть отрицательным или равняться нулю");
        }
        if( $("#age").val() < 5 || $("#age").val() > 99){
            errors.push("Допустимый возраст 5-99 лет");
        }
        if( !/^[\d]*$/.test( $("#age").val() ) ){
            errors.push("Поле Возвраст должно состоять только из цифр");
        }
        if($("#name").val().length == 0 || $("#surname").val().length == 0){
            errors.push("Не все данные заполнены");
        }
        if($("#name").val().length > 64){
            errors.push("Поле Имя не должно превышать 64 символа");
        }
        if($("#surname").val().length > 64){
            errors.push("Поле Фамилия не должно превышать 64 символа");
        }
        if(! /^([А-Я]{1}[а-яё]{1,64}|[A-Z]{1}[a-z]{1,64})*$/.test( $("#name").val() )){
            errors.push("Поле Имя должно состоять только из букв и начинаться с заглавной буквы");
        }
        if(! /^([А-Я]{1}[а-яё]{1,64}|[A-Z]{1}[a-z]{1,64})*$/.test( $("#surname").val() )){
            errors.push("Поле Фамилия должно состоять только из букв и начинаться с заглавной буквы");
        }
        if(errors.length > 0){
            let errorsText = "";
            $.each(errors,function (index,value){
                errorsText += "- " + value + "<br>";
            });
            $(".errors").html(errorsText);
            $(".errors").css("border", "1px solid red");
            return false;
        }
        else {
            $.ajax({
                type : "POST",
                url: "newuser.php",
                data: $("#form_first").serialize(),
                success: function (data){
                data = JSON.parse(data);
                console.log(data);
                if (data.success == "true")
                    alert("Успешно");
                 else
                     alert("Возникла ошибка");
                }
            });
            $("#form_first")[0].reset();
            return false;
        }
    }

    $("#save").click(save);
</script>
</body>
</html>