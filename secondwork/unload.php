<?php
include("settings/config.php");
include("quickstart.php");


$age = 18;
$stmt = $db->prepare("SELECT * FROM `user` WHERE `age` > ?");

if($stmt->execute([$age])){
    $count = 0;
    $values =  [
        ["Имя", "Фамилия", "Возраст"]
    ];
    while($row = $stmt->fetch(PDO::FETCH_LAZY)){
        $values[] =  [$row['name'], $row['surname'], $row['age']];
        $count++;
    }
    $client = getClient();
    $service = new Google_Service_Sheets($client);
    $spreadsheetId = '1i8nec-jY4ZnjUlqhOifJVKjI30_xN0WuMwGXGTXDGKI';

    $service->spreadsheets_values->clear($spreadsheetId, 'A1:D1000', new Google_Service_Sheets_ClearValuesRequest([])); // Очищаем таблицу от A1 до R100

    $range = 'A1:C'. $count;
    $body    = new Google_Service_Sheets_ValueRange( [ 'values' => $values ] );
    $options = array( 'valueInputOption' => 'RAW' );
    $service->spreadsheets_values->update( $spreadsheetId, 'A1', $body, $options );

    $savePath = __DIR__ . '/users.xlsx';

    $sheetExportUrl = 'https://docs.google.com/spreadsheets/d/' . $spreadsheetId . '/export';
    file_put_contents($savePath, file_get_contents($sheetExportUrl));

    $file = $savePath;
    header ("Content-Type: application/octet-stream");
    header ("Accept-Ranges: bytes");
    header ("Content-Length: ".filesize($file));
    header ("Content-Disposition: attachment; filename=".$file);
    readfile($file);


}
